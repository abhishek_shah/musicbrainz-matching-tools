#!/usr/bin/env python

import os
import sys
import json
import sqlite3

db = sqlite3.connect(sys.argv[1])

cursor = db.cursor()
cursor.execute("SELECT bucket, mb_release_id FROM releases")
for bucket, mb_release_id in cursor:
    discs = json.load(open(os.path.join('wcd_metadata', bucket, 'parsed_tracks.json')))
    num_tracks = 0
    num_tracks_with_mbids = 0
    for disc_no, tracks in discs.items():
        for track_no, track in tracks.items():
            num_tracks += 1
            if 'mb_recording_id' in track:
                num_tracks_with_mbids += 1
    if num_tracks != num_tracks_with_mbids:
        print bucket, mb_release_id


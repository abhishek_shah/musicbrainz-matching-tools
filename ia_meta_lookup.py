#!/usr/bin/env python

import re
import os
import sys
import json
import operator
import logging
import psycopg2
import psycopg2.extras
import psycopg2.extensions
import pprint
import sqlite3
import unicodedata
from lxml import etree
from utils import asciipunct, unaccent, format_time

logger = logging.getLogger('ia_meta_lookup')
logger.setLevel(logging.DEBUG)

audio_formats = set(['vbr mp3', 'flac', 'apple lossless audio', 'ogg vorbis'])

toc_query = """
    SELECT DISTINCT
        r.gid AS release_id,
        rn.name AS release_name,
        artist_name.name AS artist_name,
        m.id AS medium_id,
        m.position AS medium_position,
        m.name AS medium_name,
        t.id AS tracklist_id
    FROM
        medium m
        JOIN tracklist t ON t.id = m.tracklist
        JOIN tracklist_index ti ON ti.tracklist = t.id
        JOIN release r ON m.release = r.id
        JOIN release_name rn ON r.name = rn.id
        JOIN artist_credit ON r.artist_credit = artist_credit.id
        JOIN artist_name ON artist_credit.name = artist_name.id
    WHERE
        toc <@ create_bounding_cube(%(durations)s, %(fuzzy)s::int) AND
        track_count = %(num_tracks)s
"""

tracklist_query = """
    SELECT
        recording.gid AS mb_recording_id,
        track_name.name AS title,
        track.position AS track_no
    FROM
        track
        JOIN track_name ON track.name = track_name.id
        JOIN recording ON track.recording = recording.id
        JOIN track_name recording_name ON recording.name = recording_name.id
    WHERE
        track.tracklist = %(tracklist)s
    ORDER BY
        track.position
"""

medium_count_query = """
    SELECT count(*) FROM medium WHERE release = (SELECT id FROM release WHERE gid = %(release)s)
"""


def fetch_tracklist(id):
    cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(tracklist_query, dict(tracklist=id))
    tracks = {}
    for row in cursor:
        tracks[row['track_no']] = dict(row)
    return tracks


def get_medium_count(id):
    cursor = db.cursor()
    cursor.execute(medium_count_query, dict(release=id))
    row = cursor.fetchone()
    return row[0]


def lookup_disc(tracks):
    durations = [i['length'] * 1000 for i in sorted(tracks.values(), key=operator.itemgetter('track_no'))]
    toc = ' '.join(map(format_time, durations))
    logger.info('Looking up TOC %s', toc)
    if any(d >= 2 ** 32 for d in durations):
        logger.warning('Invalid TOC')
        return []
    if len(durations) <= 5:
        logger.warning('Too few tracks')
        return []
    else:
        cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute(toc_query, dict(durations=durations, fuzzy=5000, num_tracks=len(durations)))
        return cursor.fetchall()


def normalize_name(name):
    name = unicode(name)
    name = name.lower()
    name = unicodedata.normalize('NFKC', name)
    name = unaccent(name)
    name = asciipunct(name)
    return name


def remove_extra_title_information(name):
    name = re.sub(r'\((?:feat\.?|ft\.?|featuring|theme) [^)]+\)', '', name, re.U | re.I)
    name = re.sub(r'\[(?:feat\.?|ft\.?|featuring|theme) [^)]+\]', '', name, re.U | re.I)
    name = re.sub(r'\(bonus( track)?\)', '', name, re.U | re.I)
    name = re.sub(r'\s+\([^)]+\)$', '', name, re.U | re.I)
    return name


def extract_words(orig_name):
    name = ''.join(re.findall(r'\w+', orig_name, re.U))
    words = []
    for i in range(0, len(name) - 1):
        words.append(name[i:i+2])
    if not words:
        words.append(orig_name)
    return words


def iter_normalized_names(meta_a, meta_b):
    name_a = normalize_name(meta_a['title'])
    name_b = normalize_name(meta_b['title'])
    yield [name_a], [name_b]
    yield extract_words(name_a), extract_words(name_b)
    # remove extra title information from both names
    name_a = remove_extra_title_information(name_a)
    name_b = remove_extra_title_information(name_b)
    yield extract_words(name_a), extract_words(name_b)
    # remove artist name from the IA metadata
    artist = normalize_name(meta_b['artist'])
    if artist in name_b:
        x_name_b = name_b.replace(artist, '')
        yield extract_words(name_a), extract_words(x_name_b)
    if ' - ' in name_b:
        x_name_b = name_b.split(' - ', 1)[1]
        yield extract_words(name_a), extract_words(x_name_b)
    if ' / ' in name_b:
        x_name_b = name_b.split(' / ', 1)[1]
        yield extract_words(name_a), extract_words(x_name_b)


def compare_word_sets(words_a, words_b):
    word_set_a = {}
    for word in words_a:
        if word not in word_set_a:
            word_set_a[word] = 0
        word_set_a[word] += 1
    word_set_b = {}
    for word in words_b:
        if word not in word_set_b:
            word_set_b[word] = 0
        word_set_b[word] += 1
    common_words = 0
    all_words = 0
    for word in set(words_a) | set(words_b):
        count_a = word_set_a.get(word, 0)
        count_b = word_set_b.get(word, 0)
        while count_a > 0 and count_b > 0:
            all_words += 1
            common_words += 1
            count_a -= 1
            count_b -= 1
        all_words += count_a
        all_words += count_b
    score = 1.0 * common_words / all_words
    if score < 0.5:
        min_count = min(sum(word_set_a.values()), sum(word_set_b.values()))
        new_score = 1.0 * common_words / min_count
        if min_count > 6 and new_score >= 1.0:
            score = new_score
    return score


def compare_names(meta_a, meta_b):
    max_score = 0
    for a, b in iter_normalized_names(meta_a, meta_b):
        score = compare_word_sets(a, b)
        if score >= 1:
            return score
        max_score = max(score, max_score)
    return max_score


def process_bucket(base_dir):
    logger.info('Processing %s', base_dir)
    discs = {}
    for file in os.listdir(base_dir):
        if file.endswith('files.xml'):
            try:
                discs = parse_files_xml(os.path.join(base_dir, file))
            except ValueError:
                logger.exception('Error while parsing')
    logger.info(pprint.pformat(discs))
    disc_matches = {}
    common_releases = {}
    for disc_no, tracks in discs.items():
        matches = lookup_disc(tracks)
        if not matches:
            logger.info('No matching releases')
        else:
            disc_matches[disc_no] = matches
            for match in matches:
                common_releases.setdefault(match['release_id'], []).append((disc_no, match))
                logger.info('Found %r', match)
    submit = set()
    releases = set()
    file_mbids = {}
    for release_id, disc_nos in common_releases.items():
        if len(disc_nos) != len(discs):
            logger.info('Release %s matched only %d out of %d discs', release_id, len(disc_nos), len(discs))
            continue
        medium_count = get_medium_count(release_id)
        if medium_count != len(discs):
            logger.info('Directory has only %d out of %d discs on the release', len(discs), medium_count)
            continue
        logger.info('Release %s matched all discs', release_id)
        mb_discs = {}
        scores = {}
        for disc_no, match in sorted(disc_nos):
            logger.info('Evaluating %r as disc %d', match, disc_no)
            tracks = discs[disc_no]
            mb_tracks = fetch_tracklist(match['tracklist_id'])
            mb_discs[disc_no] = mb_tracks
            if mb_tracks.keys() != tracks.keys():
                logger.info('Track numbers do not match %r vs %r', mb_tracks.keys(), tracks.keys())
                continue
            num_mb_ids = 0
            track_scores = []
            for track_no in tracks.keys():
                track = tracks[track_no]
                if track.get('mb_recording_id'):
                    num_mb_ids += 1
                mb_track = mb_tracks[track_no]
                score = compare_names(mb_track, track)
                logger.debug('Track %d %s with names %r and %r = %0.2f', track_no, '**' if score <= 0.3 else '  ', mb_track['title'], track['title'], score)
                track_scores.append(score)
            if min(track_scores) > 0.3:
                scores[disc_no] = sum(track_scores)
            elif min(track_scores) > 0.1 and num_mb_ids < len(tracks):
                #sqlite_db.execute('INSERT INTO ask_later (bucket) VALUES (?)', (os.path.basename(bucket_dir),))
                print "maybe new release", bucket_dir, release_id
                #if raw_input('Score too low, match anyway? ').lower() == 'y':
                #    scores[disc_no] = sum(track_scores)
        if len(scores) == len(discs):
            logger.info('Match!')
            tracks = discs[disc_no]
            mb_tracks = mb_discs[disc_no]
            for track_no in tracks.keys():
                track = tracks[track_no]
                if track['sha1'] not in file_mbids:
                    file_mbids[track['sha1']] = []
                mb_track = mb_tracks[track_no]
                file_mbids[track['sha1']].append({
                    'mb_release_id': release_id,
                    'mb_recording_id': mb_track['mb_recording_id'],
                    'acoustid': track.get('acoustid')
                })
            releases.add(release_id)
    json.dump(discs, open(os.path.join(base_dir, 'parsed_tracks.json'), 'w'), indent=2)
    for mb_release_id in releases:
        sqlite_db.execute('INSERT INTO releases (bucket, mb_release_id) VALUES (?, ?)', (os.path.basename(bucket_dir), mb_release_id))
        print "new release", bucket_dir, mb_release_id
    for file_sha1, mbids in file_mbids.iteritems():
        for item in mbids:
            sqlite_db.execute('INSERT INTO recordings (bucket, file_sha1, mb_recording_id, mb_release_id, acoustid) VALUES (?, ?, ?, ?, ?)', (os.path.basename(bucket_dir), file_sha1, item['mb_recording_id'], item['mb_release_id'], item['acoustid']))


def parse_files_xml_file(file):
    meta = {'sha1': file.findtext('sha1')}
    if not file.findtext('length'):
        return None
    meta['length'] = int(float(file.findtext('length')) + 0.5)
    # basic metadata
    meta['album'] = file.findtext('album')
    meta['artist'] = file.findtext('artist')
    meta['title'] = file.findtext('title')
    if meta['title'] is None:
        name = os.path.basename(file.attrib['name'])
        patterns = [
            r'^\d+\.?\s*(.*?)\.(\w+)$',
            r'^\d+\s*-.*?-\(\d{4}\)-(.*?)\.(\w+)$',
            r'^\d+\s*-(.*?)\.(\w+)$',
        ]
        for pattern in patterns:
            match = re.match(pattern, name)
            if match is not None:
                meta['title'] = match.group(1)
                break
    # 
    if meta['album']:
        match = re.match(r'(.*?) \(disc (\d+)\)$', meta['album'])
        if match is not None:
            meta['album'] = match.group(1)
            meta['disc_no'] = int(match.group(2))
    # qe
    track = file.findtext('track')
    if track:
        if '/' in track:
            track = track.split('/')[0]
        meta['track_no'] = int(track)
    else:
        name = os.path.basename(file.attrib['name'])
        match = re.match(r'^(\d+)', name)
        if match is not None:
            meta['track_no'] = int(match.group(1))
    # try cdX in the directory name
    if 'disc_no' not in meta:
        name = os.path.dirname(file.attrib['name'])
        match = re.search(r'\b(?:cd|disc|disk)\s*(\d+)\b', name, re.I)
        if match is not None:
            meta['disc_no'] = int(match.group(1))
    # fallback
    if 'track_no' not in meta:
        meta['track_no'] = 1
    if 'disc_no' not in meta:
        meta['disc_no'] = 1
    # external ids
    for ext_id in file.findall('external-identifier'):
        parts = ext_id.text.split(':')
        if parts[:2] == ['urn', 'mb_recording_id']:
            meta['mb_recording_id'] = parts[2]
        elif parts[:2] == ['urn', 'acoustid'] and parts[2] != 'unknown':
            meta['acoustid'] = parts[2]
    return meta


def parse_files_xml(path):
    doc = etree.parse(path)
    files = []
    for file in doc.findall("file[@source='original']"):
        format = file.findtext('format').lower()
        if format in audio_formats:
            meta = parse_files_xml_file(file)
            if meta:
                files.append(meta)
    discs = {}
    if not files:
        return discs
    for meta in files:
        disc_no = meta.get('disc_no', 1)
        tracks = discs.setdefault(disc_no, {})
        track_no = meta.get('track_no', 1)
        if track_no in tracks:
            raise ValueError('duplicate track %d-%d' % (disc_no, track_no))
        tracks[track_no] = meta
    if min(discs.keys()) != 1 or max(discs.keys()) != len(discs):
        raise ValueError('invalid disc numbers')
    for disc_no, tracks in discs.items():
        if min(tracks.keys()) != 1 or max(tracks.keys()) != len(tracks):
            raise ValueError('invalid track numbers on disc %d' % (disc_no, ))
    return discs


#logging.basicConfig()

import config

db = psycopg2.connect(user=config.MB_DB_USER, database=config.MB_DB_NAME, host=config.MB_DB_HOST, password=config.MB_DB_PASSWORD)
db.set_client_encoding('UTF8')

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

sqlite_db = sqlite3.connect('matches_1.db')

processed = set()
for bucket, in sqlite_db.execute('SELECT bucket FROM processed'):
    processed.add(bucket)

has_release = set()
for bucket, in sqlite_db.execute('SELECT bucket FROM releases'):
    has_release.add(bucket)

should_ask = set()
for bucket, in sqlite_db.execute('SELECT bucket FROM ask_later'):
    should_ask.add(bucket)

cursor = db.cursor()
cursor.execute("SET search_path TO musicbrainz, public")

base_dir = 'wcd_metadata'
for bucket_dir in os.listdir(base_dir):
    if bucket_dir in processed or bucket_dir in has_release or bucket_dir not in should_ask:
        continue
    #print bucket_dir
    fh = logging.FileHandler(os.path.join(base_dir, bucket_dir, 'ia_meta_lookup_1.log'), 'w', encoding='utf8')
    fh.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))
    logger.addHandler(fh)
    try:
        process_bucket(os.path.join(base_dir, bucket_dir))
    finally:
        logger.removeHandler(fh)
    #if raw_input('Continue? [Y/n]') == 'n':
    #    break
    sqlite_db.execute('INSERT INTO processed (bucket) VALUES (?)', (bucket_dir,))
    sqlite_db.commit()

